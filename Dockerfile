
FROM node:12.2.0


WORKDIR /app


ENV PATH /app/node_modules/.bin:$PATH


COPY package.json /app/package.json
RUN npm install -g @angular/cli@7.3.9
RUN npm install


COPY . /app

EXPOSE 4200

CMD ["ng", "serve"]